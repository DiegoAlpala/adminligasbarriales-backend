package com.ligasbarriales.repositorio;

import java.util.Optional;

import com.ligasbarriales.modelo.Categoria;

public interface RepositorioCategoria extends RepositorioBase<Categoria> {

	Optional<Categoria> findByNombre(String nombre);


}
