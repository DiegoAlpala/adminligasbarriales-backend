package com.ligasbarriales.repositorio;

import java.util.Optional;

import com.ligasbarriales.modelo.Equipo;

public interface RepositorioEquipo extends RepositorioBase<Equipo> {


	Optional<Equipo> findByNombre(String nombre);
}
