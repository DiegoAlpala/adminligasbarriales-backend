package com.ligasbarriales.repositorio;

import com.ligasbarriales.modelo.Usuario;

public interface RepositorioUsuario extends RepositorioBase<Usuario>{

	Usuario findOneByNombreUsuario(String name);
}
