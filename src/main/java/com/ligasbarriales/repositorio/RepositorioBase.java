package com.ligasbarriales.repositorio;

import com.ligasbarriales.modelo.EntidadBase;

public interface RepositorioBase <T extends EntidadBase> extends RepositorioBaseId<T> {

}
