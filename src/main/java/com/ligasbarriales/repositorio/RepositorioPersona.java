package com.ligasbarriales.repositorio;

import java.util.Optional;

import com.ligasbarriales.modelo.Persona;

//@Repository no es necesario por que extiende de JpaRepository
public interface RepositorioPersona extends RepositorioBase<Persona> {

	Optional<Persona> findByIdentificacionNumeroIgnoreCase(String identificacionNumero);

}
