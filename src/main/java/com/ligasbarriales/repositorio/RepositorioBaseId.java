package com.ligasbarriales.repositorio;

import org.springframework.data.repository.CrudRepository;

import com.ligasbarriales.modelo.EntidadBaseId;

public interface RepositorioBaseId<T extends EntidadBaseId> extends CrudRepository<T, Long> {

}
