package com.ligasbarriales.controlador;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ligasbarriales.modelo.Jugador;
import com.ligasbarriales.servicio.IJugadorServicio;

@RestController
@RequestMapping("/jugadores")
public class JugadorController {

	@Autowired
	private IJugadorServicio servicio;
	
	@GetMapping
	public ResponseEntity<List<Jugador>> listarJugadores() {
		List<Jugador> lista = servicio.listar();
		return new ResponseEntity<List<Jugador>>(lista, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Jugador> crearJugador( @Valid @RequestBody Jugador jugador){
		Jugador jugadorCreado = servicio.registrar(jugador);
		return new ResponseEntity<Jugador>(jugadorCreado, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Jugador> modificarJugador( @Valid @RequestBody Jugador jugador){
		Jugador jugadorCreado = servicio.modificar(jugador);
		return new ResponseEntity<Jugador>(jugadorCreado, HttpStatus.OK);
	}
}
