package com.ligasbarriales.controlador;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ligasbarriales.modelo.Categoria;
import com.ligasbarriales.servicio.ICategoriaServicio;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {

	@Autowired
	private ICategoriaServicio servicio;
	
	@GetMapping
	public ResponseEntity<List<Categoria>> listarCategorias() {
		List<Categoria> lista = servicio.listar();
		return new ResponseEntity<List<Categoria>>(lista, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Categoria> crearCategoria( @Valid @RequestBody Categoria categoria){
		Categoria categoriaCreada = servicio.registrar(categoria);
		return new ResponseEntity<Categoria>(categoriaCreada, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Categoria> modificarCategoria( @Valid @RequestBody Categoria catgoria){
		Categoria catgoriaModificada = servicio.modificar(catgoria);
		return new ResponseEntity<Categoria>(catgoriaModificada, HttpStatus.OK);
	}
}
