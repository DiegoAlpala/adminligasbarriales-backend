package com.ligasbarriales.controlador;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ligasbarriales.modelo.Equipo;
import com.ligasbarriales.servicio.IEquipoServicio;

@RestController
@RequestMapping("/equipos")
public class EquipoController {

	@Autowired
	private IEquipoServicio servicio;
	
	@GetMapping
	public ResponseEntity<List<Equipo>> listarEquipos() {
		List<Equipo> lista = servicio.listar();
		return new ResponseEntity<List<Equipo>>(lista, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Equipo> crearEquipo( @Valid @RequestBody Equipo equipo){
		Equipo equipoCreado = servicio.registrar(equipo);
		return new ResponseEntity<Equipo>(equipoCreado, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Equipo> modificarEquipo( @Valid @RequestBody Equipo equipo){
		Equipo equipoModificado = servicio.modificar(equipo);
		return new ResponseEntity<Equipo>(equipoModificado, HttpStatus.OK);
	}
}
