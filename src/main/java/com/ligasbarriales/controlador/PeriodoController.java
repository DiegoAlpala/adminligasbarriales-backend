package com.ligasbarriales.controlador;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ligasbarriales.modelo.Periodo;
import com.ligasbarriales.servicio.IPeriodoServicio;

@RestController
@RequestMapping("/periodos")
public class PeriodoController {

	@Autowired
	private IPeriodoServicio servicio;
	
	@GetMapping
	public ResponseEntity<List<Periodo>> listarPeriodos() {
		List<Periodo> lista = servicio.listar();
		return new ResponseEntity<List<Periodo>>(lista, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Periodo> crearPeriodo( @Valid @RequestBody Periodo periodo){
		Periodo periodoCreado = servicio.registrar(periodo);
		return new ResponseEntity<Periodo>(periodoCreado, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<Periodo> modificarPeriodo( @Valid @RequestBody Periodo periodo){
		Periodo periodoModificado = servicio.modificar(periodo);
		return new ResponseEntity<Periodo>(periodoModificado, HttpStatus.OK);
	}
}
