package com.ligasbarriales.modelo;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "calificacion")
public class Calificacion extends EntidadBase {
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@NotNull
	private Equipo equipo;
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@NotNull
	private Jugador jugador;
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@NotNull
	private Categoria categoria;
	
	@NotNull
	@NotBlank
	@Column(name="numero")
	private int numero;
	
	@Column(name="generado_carnet")
	private Boolean generadoCarnet;
	
	@Column(name="fecha_generado")
	private LocalDateTime fechaGenerado;
	
	@Column(name="fecha_calificado")
	private LocalDateTime fechaCalificado;

	public Calificacion( Equipo equipo,  Jugador jugador,  Categoria categoria, int numero) {
		super();
		this.equipo = equipo;
		this.jugador = jugador;
		this.categoria = categoria;
		this.numero = numero;
		this.generadoCarnet = Boolean.FALSE;
		this.fechaCalificado = LocalDateTime.now();
	}

	public Equipo getEquipo() {
		return equipo;
	}

	public Jugador getJugador() {
		return jugador;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public int getNumero() {
		return numero;
	}

	public Boolean getGeneradoCarnet() {
		return generadoCarnet;
	}

	public LocalDateTime getFechaGenerado() {
		return fechaGenerado;
	}

	public LocalDateTime getFechaCalificado() {
		return fechaCalificado;
	}

}
