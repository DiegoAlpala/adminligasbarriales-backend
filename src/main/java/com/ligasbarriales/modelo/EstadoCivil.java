package com.ligasbarriales.modelo;

public enum EstadoCivil {

	CASADO("Casado"),
	DIVORCIADO("Divorciado"),
	OTRO("Otro"),
	SOLTERO("Soltero"),
	UNION_LIBRE("Unión Libre"),
	VIUDO("Viudo");
	
	private String descripcion;
	

	private EstadoCivil(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

}
