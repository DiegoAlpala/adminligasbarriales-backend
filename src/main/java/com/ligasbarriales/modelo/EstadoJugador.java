package com.ligasbarriales.modelo;

public enum EstadoJugador {

	HABILITADO("Habilitado"),
	SUSPENDIDO("Suspendido"),
	INHANILITADO("Inhabilitado");
	
	private String descripcion;
	
	private EstadoJugador(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	
}
