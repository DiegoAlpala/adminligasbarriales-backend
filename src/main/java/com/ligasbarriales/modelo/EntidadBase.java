package com.ligasbarriales.modelo;

import java.time.LocalDate;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@SuppressWarnings("serial")
public abstract class EntidadBase extends EntidadBaseId {

	public static final String PROPIEDAD_FECHA_CREACION = "fechaCreacion";
	public static final String PROPIEDAD_FECHA_MODIFICACION = "fechaModificacion";
	
	@CreatedDate
	private LocalDate creadoFecha;
	
	@CreatedBy
	private String creadoPor;
	
	@LastModifiedDate
	private LocalDate modificadoFecha;
	
	@LastModifiedBy
	private String modificadoPor;

	public AuditoriaModificado getAuditoriaModificado() {	
		return new AuditoriaModificado(getModificadoPor(), getModificadoFecha());
	}

	public LocalDate getCreadoFecha() {
		return creadoFecha;
	}

	public String getCreadoPor() {
		return creadoPor;
	}
	
	public LocalDate getModificadoFecha() {
		return modificadoFecha;
	}

	public String getModificadoPor() {
		return modificadoPor;
	}

}
