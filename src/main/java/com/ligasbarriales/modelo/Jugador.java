package com.ligasbarriales.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name="jugador")
public class Jugador extends EntidadBase {
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="estado", nullable = false, length = 64)
	private EstadoJugador estadoJugador;
	
	@OneToOne
	@JoinColumn(name="persona_id", referencedColumnName = "id")
	private Persona persona;
	

	public Jugador() {}
	
	public Jugador(Persona persona) {
		super();
		this.persona = persona;
		this.estadoJugador = EstadoJugador.HABILITADO;
	}

	public EstadoJugador getEstadoJugador() {
		return estadoJugador;
	}

	public Persona getPersona() {
		return persona;
	}

	@Override
	public String toString() {
		return "Jugador [estadoJugador=" + estadoJugador + ", jugador=" + persona + "]";
	}
}
