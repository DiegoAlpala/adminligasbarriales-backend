package com.ligasbarriales.modelo;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "periodo_equipo")
public class PeriodoEquipo extends EntidadBase {

	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@NotNull
	private Periodo periodo;
	
	@ManyToOne(optional = false, fetch = FetchType.EAGER)
	@NotNull
	private Equipo equipo;

	public PeriodoEquipo(Periodo periodo, Equipo equipo) {
		super();
		this.periodo = periodo;
		this.equipo = equipo;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public Equipo getEquipo() {
		return equipo;
	}
	
}
