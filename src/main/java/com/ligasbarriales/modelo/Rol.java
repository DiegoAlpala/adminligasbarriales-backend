package com.ligasbarriales.modelo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name = "rol")
public class Rol extends EntidadBase {
	
	@Column(name = "nombre", nullable = false, length = 64)
	private String nombre;
	
	@Column(name = "descripcion", nullable = false, length = 64)
	private String descripcion;
	
	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "rol_menu", joinColumns= @JoinColumn(name="rol_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name="menu_id", referencedColumnName = "id"))
	private List<Menu> listaMenu;


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	

}
