package com.ligasbarriales.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuppressWarnings("serial")
@Entity
@Table(name = "categoria")
public class Categoria extends EntidadBase {
	
	@NotNull
	@NotBlank
	@Size(min = 3, message = "Al menos debe tener 3 caracteres")
	@Column(name = "nombre", length = 128, unique = true)
	private String nombre;
	
	@Column(name = "descripcion", length = 512)
	private String descripcion;
	
	@Column(name = "activo")
	private Boolean activo;
	

	public Categoria(String nombre, String descripcion) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.activo = true;
	}

	public String getNombre() {
		return nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public Boolean getActivo() {
		return activo;
	}

	@Override
	public String toString() {
		return "Categoria [nombre=" + nombre + ", descripcion=" + descripcion + ", activo=" + activo + "]";
	}
	
	
		
}
