package com.ligasbarriales.modelo;

public enum TipoIdentificacion {

	CEDULA( "Cédula"),
	PASAPORTE("Pasaporte");
	
	private String descripcion;
	
	private TipoIdentificacion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
}
