package com.ligasbarriales.modelo;

public enum Estado {

	ACTIVO("Activo"),
	INACTIVO("Inactivo"),
	SUSPENDIDO("Suspendido"),
	EXPULSADO("Expulsado");
	
	private String descripcion;
	
	private Estado(String descricpion) {
		this.descripcion = descricpion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	
}
