package com.ligasbarriales.modelo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@SuppressWarnings("serial")
@Entity
@Table(name = "periodo")
public class Periodo extends EntidadBase {


	@NotNull
	@NotBlank
	@Column(name = "codigo", length = 64, unique = true)
	private String codigo;
	
	@NotNull
	@NotBlank
	@Column(name = "anio", nullable = false)
	private int anio;
	
	@NotNull
	@NotBlank
	@Column(name = "actual", nullable = false)
	private boolean actual;

	
	

}
