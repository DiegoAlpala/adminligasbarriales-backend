package com.ligasbarriales.modelo;

public enum Genero {

	MASCULINO("Masculino"),
	FEMENINO("Femenino");
	
	private String descripcion;
	
	private Genero(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
}
