package com.ligasbarriales.modelo;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@SuppressWarnings("serial")
@Entity
@Table(name="equipo")
public class Equipo extends EntidadBase {

	@NotNull
	@NotBlank
	@Size(min = 3, message = "Nombre debe contener al menos 3 caracteres")
	@Column(name="nombre", length = 128)
	private String nombre;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name="estado",  length = 64)
	private EstadoEquipo estadoEquipo;
	
	@Column(name="anio_fundacion")
	private LocalDate anioFundacion;

	public Equipo() {}
	
	public Equipo(String nombre, LocalDate anioFundacion) {
		this.nombre = nombre;
		this.estadoEquipo = EstadoEquipo.ACTIVO;
		this.anioFundacion = anioFundacion;
	}

	public String getNombre() {
		return nombre;
	}

	public EstadoEquipo getEstadoEquipo() {
		return estadoEquipo;
	}

	public LocalDate getAnioFundacion() {
		return anioFundacion;
	}		
	
	
}
