package com.ligasbarriales.modelo;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;


@SuppressWarnings("serial")
@Entity
@Table(name = "persona")
public class Persona extends EntidadBase {

	@NotNull
	@NotBlank
	@Size(min=3, message="Nombre Primero debe tener mínimo 3 caracteres")
	@Column(name = "nombre_primero", nullable = false, length = 512)
	private String nombrePrimero;
	
	@Column(name = "nombre_segundo", nullable = false, length = 512)
	private String nombreSegundo;

	@NotNull
	@NotBlank
	@Size(min=3, message="Apellido debe tener mínimo 3 caracteres")
	@Column(name = "apellido_primer", nullable = false, length = 512)
	private String apellidoPrimero;
	
	@Column(name = "apellido_segundo", nullable = false, length = 512)
	private String apellidoSegundo;

	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_identificacion", nullable = false, length = 64)
	private TipoIdentificacion tipoIdentificacion;
	

	@NotNull
	@NotBlank
	@Column(name = "identificacion_numero",  length = 28)
	private String identificacionNumero;

	@NotNull
	@Column(name = "fecha_nacimiento")
	private LocalDate fechaNacimiento;

	@Size(min=10, max=10 , message="Celular debe tener mínimo 10 caracteres")
	@Column(name = "celular", nullable = true, length = 10)
	private String celular;

	@Email
	@Column(name = "correo", nullable = true, length = 512)
	private String correo;

	@Enumerated(EnumType.STRING)
	@Column(name = "estado_civil",  length = 64)
	private EstadoCivil estadoCivil;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "genero", length =32 )
	private Genero genero;
		
	@Enumerated(EnumType.STRING)
	@NotNull
	@Column(name = "estado", length = 64)
	private Estado estado;
	
	@Column(name = "foto_ruta", length = 2048)
	private String fotoRuta;
	
	public Persona() {}
	
	public Persona(
			String nombrePrimero,
			String nombreSegundo,
			String apellidoPrimero,
			String apellidoSegundo, 
			TipoIdentificacion tipoIdentificacion,
			String identificacionNumero, 
			LocalDate fechaNacimiento,
			String celular,
			String correo, 
			EstadoCivil estadoCivil, 
			Genero genero) {
		
		super();
		this.nombrePrimero = nombrePrimero;
		this.nombreSegundo = nombreSegundo;
		this.apellidoPrimero = apellidoPrimero;
		this.apellidoSegundo = apellidoSegundo;
		this.tipoIdentificacion = tipoIdentificacion;
		this.identificacionNumero = identificacionNumero;
		this.fechaNacimiento = fechaNacimiento;
		this.celular = celular;
		this.correo = correo;
		this.estadoCivil = estadoCivil;
		this.genero = genero;
		this.estado = Estado.ACTIVO;
	}

	public String getNombrePrimero() {
		return nombrePrimero;
	}

	public String getNombreSegundo() {
		return nombreSegundo;
	}

	public String getApellidoPrimero() {
		return apellidoPrimero;
	}

	public String getApellidoSegundo() {
		return apellidoSegundo;
	}
	
	public String getNombreCompleto() {
		return String.format("%s %s %s %s", this.apellidoPrimero, this.apellidoSegundo, this.nombrePrimero, this.nombreSegundo);
	}

	public TipoIdentificacion getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public String getIdentificacionNumero() {
		return identificacionNumero;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public String getCelular() {
		return celular;
	}

	public String getCorreo() {
		return correo;
	}

	public EstadoCivil getEstadoCivil() {
		return estadoCivil;
	}

	public Genero getGenero() {
		return genero;
	}

	public Estado getEstado() {
		return estado;
	}
	
	public void cambiarFoto(String ruta) {
		this.fotoRuta = ruta;
	}
	
	public String getRutaFoto() {
		return this.fotoRuta;
	}
	
	public boolean tieneFoto() {
		return StringUtils.isNotEmpty(this.fotoRuta) ;
	}

	@Override
	public String toString() {
		return "Persona [nombrePrimero=" + nombrePrimero + ", nombreSegundo=" + nombreSegundo + ", apellidoPrimero="
				+ apellidoPrimero + ", apellidoSegundo=" + apellidoSegundo + ", tipoIdentificacion="
				+ tipoIdentificacion + ", identificacionNumero=" + identificacionNumero + ", fechaNacimiento="
				+ fechaNacimiento + ", celular=" + celular + ", correo=" + correo + ", estadoCivil=" + estadoCivil
				+ ", genero=" + genero + ", estado=" + estado + "]";
	}
	
		
}
