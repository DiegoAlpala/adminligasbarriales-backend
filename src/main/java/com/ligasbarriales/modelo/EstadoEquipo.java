package com.ligasbarriales.modelo;

public enum EstadoEquipo {

	ACTIVO("Activo"),
	DESERTADO("Desertado");
	
	private String descripcion;
	
	private EstadoEquipo(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcion() {
		return descripcion;
	}
	
	
}
