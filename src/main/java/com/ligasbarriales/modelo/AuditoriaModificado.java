package com.ligasbarriales.modelo;

import java.time.LocalDate;

public class AuditoriaModificado {
	
	private LocalDate fecha;
	private String por;

	public AuditoriaModificado(String por, LocalDate fecha) {
		this.por = por;
		this.fecha = fecha;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public String getPor() {
		return por;
	}
}
