package com.ligasbarriales.servicio;

import java.util.List;

import com.ligasbarriales.modelo.Menu;

public interface IMenuServicio extends ICRUD<Menu, Integer>{

	List<Menu> listarMenuPorUsuario(String usuario);
}
