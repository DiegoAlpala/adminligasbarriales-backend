package com.ligasbarriales.servicio;

import com.ligasbarriales.modelo.Categoria;

public interface ICategoriaServicio extends ICRUD<Categoria, Long> {

}
