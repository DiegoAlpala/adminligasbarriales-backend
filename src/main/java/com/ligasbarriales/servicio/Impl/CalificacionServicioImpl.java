package com.ligasbarriales.servicio.Impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ligasbarriales.modelo.Calificacion;
import com.ligasbarriales.repositorio.RepositorioCalificacion;
import com.ligasbarriales.servicio.ICalificacionServicio;

@Service
public class CalificacionServicioImpl implements ICalificacionServicio {

	private static final Log LOG = LogFactory.getLog(CalificacionServicioImpl.class);

	@Autowired
	private RepositorioCalificacion repositorioCalificacion;
	
	@Override
	public Calificacion registrar(Calificacion obj) {
		LOG.info("Guardando Calificacion nueva: "+ obj.toString());
		return repositorioCalificacion.save(obj);
	}

	@Override
	public Calificacion modificar(Calificacion obj) {
		LOG.info("Actualizando Calificacion: "+ obj.toString());
		return repositorioCalificacion.save(obj);
	}

	@Override
	public List<Calificacion> listar() {
		LOG.info("Buscando Calificaciones... ");
		List<Calificacion> calificacionesTodos = (List<Calificacion>) repositorioCalificacion.findAll();
		LOG.info(String.format("Obteniendo %s Calificacions: ", calificacionesTodos.size()));
		return calificacionesTodos;
	}

	@Override
	public Optional<Calificacion> listarPorId(Long id) {
		LOG.info(String.format("Buscando calificacion por Id %s. ",id));
		return repositorioCalificacion.findById(id);
	}

	@Override
	public boolean eliminar(Long id) {
		LOG.info(String.format("Eliminando calificacion con id %s. ",id));
		repositorioCalificacion.deleteById(id);
		return true;
	}
	
	

}
