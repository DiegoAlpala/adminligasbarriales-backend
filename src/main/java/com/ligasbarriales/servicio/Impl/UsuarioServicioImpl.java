package com.ligasbarriales.servicio.Impl;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ligasbarriales.modelo.Usuario;
import com.ligasbarriales.repositorio.RepositorioUsuario;

@Service
public class UsuarioServicioImpl implements UserDetailsService{

	@Autowired
	private RepositorioUsuario repositorioUsuario;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		Usuario usuario = repositorioUsuario.findOneByNombreUsuario(username);
		
		if(usuario == null) {
			throw new UsernameNotFoundException(String.format("Usuario no existe", username));
		}
		
		List<GrantedAuthority> roles = new ArrayList<>();
		
		usuario.getRoles().forEach(rol ->{
			roles.add(new SimpleGrantedAuthority(rol.getNombre()));
		});

		return new User(usuario.getNombreUsuario(), usuario.getClave(), roles);
	}

}
