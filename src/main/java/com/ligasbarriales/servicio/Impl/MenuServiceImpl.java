package com.ligasbarriales.servicio.Impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ligasbarriales.modelo.Menu;
import com.ligasbarriales.repositorio.RepositorioMenu;
import com.ligasbarriales.servicio.IMenuServicio;

@Service
public class MenuServiceImpl implements IMenuServicio {

	@Autowired
	private RepositorioMenu repo;
	
	@Override
	public Menu registrar(Menu obj) {
		
		return repo.save(obj);
	}

	@Override
	public Menu modificar(Menu obj) {
		
		return repo.save(obj);
	}

	@Override
	public List<Menu> listar() {
		
		return repo.findAll();
	}

	@Override
	public Optional<Menu> listarPorId(Integer id) {
		
		return repo.findById(id);
	}

	@Override
	public boolean eliminar(Integer id) {
		repo.deleteById(id);
		return true;
	}

	@Override
	public List<Menu> listarMenuPorUsuario(String usuario) {
		List<Menu> menus = new ArrayList<>();
		repo.listarMenuPorUsuario(usuario).forEach(x -> {
			Menu m = new Menu();
			m.setIDMenu(Long.parseLong(String.valueOf(x[0])));
			m.setIcono(String.valueOf(x[1]));
			m.setNombre(String.valueOf(x[2]));
			m.setUrl(String.valueOf(x[3]));
			menus.add(m);
		});
		return menus;
	}

}
