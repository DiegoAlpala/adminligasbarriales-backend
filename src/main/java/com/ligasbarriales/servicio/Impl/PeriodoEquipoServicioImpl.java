package com.ligasbarriales.servicio.Impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ligasbarriales.modelo.PeriodoEquipo;
import com.ligasbarriales.repositorio.RepositorioPeriodoEquipo;
import com.ligasbarriales.servicio.IPeriodoEquipoServicio;

@Service
public class PeriodoEquipoServicioImpl implements IPeriodoEquipoServicio {

	private static final Log LOG = LogFactory.getLog(PeriodoEquipoServicioImpl.class);

	@Autowired
	private RepositorioPeriodoEquipo repositorioPeriodoEquipo;
	
	@Override
	public PeriodoEquipo registrar(PeriodoEquipo obj) {
		LOG.info("Guardando PeriodoEquipo nuevo: "+ obj.toString());
		return repositorioPeriodoEquipo.save(obj);
	}

	@Override
	public PeriodoEquipo modificar(PeriodoEquipo obj) {
		LOG.info("Actualizando PeriodoEquipo: "+ obj.toString());
		return repositorioPeriodoEquipo.save(obj);
	}

	@Override
	public List<PeriodoEquipo> listar() {
		LOG.info("Buscando PeriodoEquipos... ");
		List<PeriodoEquipo> periodosTodos = (List<PeriodoEquipo>) repositorioPeriodoEquipo.findAll();
		LOG.info(String.format("Obteniendo %s PeriodoEquipos: ", periodosTodos.size()));
		return periodosTodos;
	}

	@Override
	public Optional<PeriodoEquipo> listarPorId(Long id) {
		LOG.info(String.format("Buscando PeriodoEquipo por Id %s. ",id));
		return repositorioPeriodoEquipo.findById(id);
	}

	@Override
	public boolean eliminar(Long id) {
		LOG.info(String.format("Eliminando PeriodoEquipo con id %s. ",id));
		repositorioPeriodoEquipo.deleteById(id);
		return true;
	}

}
