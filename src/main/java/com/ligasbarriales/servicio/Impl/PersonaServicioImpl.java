package com.ligasbarriales.servicio.Impl;

import static com.ligasbarriales.utils.UtilidadesCadena.noEsNuloNiBlanco;
import static com.ligasbarriales.utils.UtilidadesCadena.normalizarNombre;
import static com.ligasbarriales.utils.UtilidadesCadena.normalizarNumeroIdentificacion;
import static com.ligasbarriales.utils.validacionNumeroIdentificacion.ValidacionNumeroIdentificacion.validar;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ligasbarriales.exception.PersonaIdentificacionNumeroInvalidoException;
import com.ligasbarriales.modelo.Persona;
import com.ligasbarriales.modelo.TipoIdentificacion;
import com.ligasbarriales.repositorio.RepositorioPersona;
import com.ligasbarriales.servicio.IPersonaServicio;

@Service
public class PersonaServicioImpl implements IPersonaServicio {

	private static final Log LOG = LogFactory.getLog(PersonaServicioImpl.class);

	@Autowired
	private RepositorioPersona repositorioPersona;
	
	@Override
	public Persona registrar(Persona obj) {
		
		final String numeroidentificacionNormalizado = asegurarIdentificacionNumeroValido(obj.getTipoIdentificacion(), obj.getIdentificacionNumero());
		final String nombrePrimeroNormalizado = asegurarNombrePrimeroValido(obj.getNombrePrimero());
		final String nombreSegundoNormalizado = asegurarNombreSegundoValido(obj.getNombreSegundo());
		final String apellidoPrimeroNormalizado = asegurarApellidoPrimeroValido(obj.getApellidoPrimero());
		final String apellidoSegundoNormalizado = asegurarApellidoSegundoValido(obj.getApellidoSegundo());
		
		LOG.info("Guardando Persona Nueva: " + obj.toString());
		return repositorioPersona.save(new Persona(nombrePrimeroNormalizado, nombreSegundoNormalizado,
				apellidoPrimeroNormalizado,apellidoSegundoNormalizado, obj.getTipoIdentificacion(),
				numeroidentificacionNormalizado, obj.getFechaNacimiento(), obj.getCelular(), obj.getCorreo(),
				obj.getEstadoCivil(), obj.getGenero()));
	}

	@Override
	public Persona modificar(Persona obj) {
		LOG.info("Actualizando Persona: "+ obj.toString());
		return repositorioPersona.save(obj);
	}

	@Override
	public List<Persona> listar() {
		LOG.info("Buscando Personas... ");
		List<Persona> personasTodas = (List<Persona>) repositorioPersona.findAll();
		LOG.info(String.format("Obteniendo %s Personas: ", personasTodas.size()));
		return personasTodas;
	}

	@Override
	public Optional<Persona> listarPorId(Long id) {
		LOG.info(String.format("Buscando a persona por Id %s. ",id));
		return repositorioPersona.findById(id);
	}

	@Override
	public boolean eliminar(Long id) {
		LOG.info(String.format("Eliminando a Persona con id %s. ",id));
		repositorioPersona.deleteById(id);
		return true;
	}
	
	private String asegurarIdentificacionNumeroValido(TipoIdentificacion tipo, String identificacionNumero) {
		
		final String numeroIdentificacionNormalizado = normalizarNumeroIdentificacion(identificacionNumero);
		
		if (!validar(tipo, numeroIdentificacionNormalizado)) {
			LOG.error(String.format("Número de indentificación inválido %s, %s. ",tipo, identificacionNumero ));
			throw new PersonaIdentificacionNumeroInvalidoException(numeroIdentificacionNormalizado);
		}
		
		return numeroIdentificacionNormalizado;
	}
	
	private String asegurarNombrePrimeroValido(String nombrePrimero) {
		
		return normalizarNombre(nombrePrimero);		
	}
	
	private String asegurarApellidoPrimeroValido(String apellidoPrimero) {
		
		return  normalizarNombre(apellidoPrimero);		
	}
	
	private String asegurarNombreSegundoValido(String nombreSegundo) {
		
		String nombreSegundoNormalizado = "";		
		
		if(noEsNuloNiBlanco(nombreSegundo)) {
			nombreSegundoNormalizado = normalizarNombre(nombreSegundo);
		}
		
		return nombreSegundoNormalizado;
	}
	
	private String asegurarApellidoSegundoValido(String apellidoSegundo) {
		
		String apellidoSegundoNormalizado = "";		
		
		if(noEsNuloNiBlanco(apellidoSegundo)) {
			apellidoSegundoNormalizado = normalizarNombre(apellidoSegundo);
		}
		
		return apellidoSegundoNormalizado;
				
	}
	
	

}
