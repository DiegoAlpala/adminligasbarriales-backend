package com.ligasbarriales.servicio.Impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.ligasbarriales.modelo.Jugador;
import com.ligasbarriales.modelo.Persona;
import com.ligasbarriales.repositorio.RepositorioJugador;
import com.ligasbarriales.servicio.IJugadorServicio;
import com.ligasbarriales.servicio.IPersonaServicio;

@Service
public class JugadorServicioImpl implements IJugadorServicio {

	private static final Log LOG = LogFactory.getLog(JugadorServicioImpl.class);

	@Autowired
	private RepositorioJugador repositorioJugador;
	
	@Autowired
	private IPersonaServicio personaServicio;
	
	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	@Override
	public Jugador registrar(Jugador obj) {
		
		Persona personaCreada = guardarPersona(obj);
		
		if(personaCreada.getId()> 0) {			
			LOG.info("Guardando Jugador nuevo: "+ obj.toString());
			return repositorioJugador.save(new Jugador(personaCreada));
		}
		return null;
	}
	
	private Persona guardarPersona(Jugador dto) {
		return personaServicio.registrar(dto.getPersona());
	}

	@Override
	public Jugador modificar(Jugador obj) {
		LOG.info("Actualizando Jugador: "+ obj.toString());
		return repositorioJugador.save(obj);
	}

	@Override
	public List<Jugador> listar() {
		LOG.info("Buscando Jugadores... ");
		List<Jugador> jugadoresTodos = (List<Jugador>) repositorioJugador.findAll();
		LOG.info(String.format("Obteniendo %s Jugadores: ", jugadoresTodos.size()));
		return jugadoresTodos;
	}

	@Override
	public Optional<Jugador> listarPorId(Long id) {
		LOG.info(String.format("Buscando a jugador por Id %s. ",id));
		return repositorioJugador.findById(id);
	}

	@Override
	public boolean eliminar(Long id) {
		LOG.info(String.format("Eliminando a Jugador con id %s. ",id));
		repositorioJugador.deleteById(id);
		return true;
	}

}
