package com.ligasbarriales.servicio.Impl;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ligasbarriales.modelo.Periodo;
import com.ligasbarriales.repositorio.RepositorioPeriodo;
import com.ligasbarriales.servicio.IPeriodoServicio;

@Service
public class PeriodoServicioImpl implements IPeriodoServicio {

	private static final Log LOG = LogFactory.getLog(PeriodoServicioImpl.class);

	@Autowired
	private RepositorioPeriodo repositorioPeriodo;
	
	@Override
	public Periodo registrar(Periodo obj) {
		LOG.info("Guardando Periodo nuevo: "+ obj.toString());
		return repositorioPeriodo.save(obj);
	}

	@Override
	public Periodo modificar(Periodo obj) {
		LOG.info("Actualizando Periodo: "+ obj.toString());
		return repositorioPeriodo.save(obj);
	}

	@Override
	public List<Periodo> listar() {
		LOG.info("Buscando Periodos... ");
		List<Periodo> periodosTodos = (List<Periodo>) repositorioPeriodo.findAll();
		LOG.info(String.format("Obteniendo %s Periodos: ", periodosTodos.size()));
		return periodosTodos;
	}

	@Override
	public Optional<Periodo> listarPorId(Long id) {
		LOG.info(String.format("Buscando Periodo por Id %s. ",id));
		return repositorioPeriodo.findById(id);
	}

	@Override
	public boolean eliminar(Long id) {
		LOG.info(String.format("Eliminando Periodo con id %s. ",id));
		repositorioPeriodo.deleteById(id);
		return true;
	}

}
