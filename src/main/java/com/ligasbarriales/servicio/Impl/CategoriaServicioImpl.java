package com.ligasbarriales.servicio.Impl;

import static com.ligasbarriales.utils.UtilidadesCadena.normalizarNombre;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ligasbarriales.exception.CategoriaYaExisteException;
import com.ligasbarriales.modelo.Categoria;
import com.ligasbarriales.repositorio.RepositorioCategoria;
import com.ligasbarriales.servicio.ICategoriaServicio;

@Service
public class CategoriaServicioImpl implements ICategoriaServicio {

	private static final Log LOG = LogFactory.getLog(CategoriaServicioImpl.class);

	@Autowired
	private RepositorioCategoria repositorioCategoria;
	
	@Override
	public Categoria registrar(Categoria obj) {
		
		asegurarUnicaCategoria(obj.getNombre());		
		String nombreNormalizado = asegurarNombre(obj.getNombre());
		
		Categoria categoria = new Categoria(nombreNormalizado, obj.getDescripcion());		
		LOG.info("Guardando Categoria nueva: "+ categoria.toString());
		
		return repositorioCategoria.save(categoria);
	}

	@Override
	public Categoria modificar(Categoria obj) {
		LOG.info("Actualizando Categoria: "+ obj.toString());
		return repositorioCategoria.save(obj);
	}

	@Override
	public List<Categoria> listar() {
		LOG.info("Buscando Categorias... ");
		List<Categoria> categoriasTodas = (List<Categoria>) repositorioCategoria.findAll();
		LOG.info(String.format("Obteniendo %s Categorias: ", categoriasTodas.size()));
		return categoriasTodas;
	}

	@Override
	public Optional<Categoria> listarPorId(Long id) {
		LOG.info(String.format("Buscando a persona por Id %s. ",id));
		return repositorioCategoria.findById(id);
	}

	@Override
	public boolean eliminar(Long id) {
		LOG.info(String.format("Eliminando a Categoria con id %s. ",id));
		repositorioCategoria.deleteById(id);
		return true;
	}
	
	private String asegurarNombre(String nombre) {
		return normalizarNombre(nombre);	
	}
	
	private void asegurarUnicaCategoria(String nombre){
		Optional<Categoria> categoria = repositorioCategoria.findByNombre(nombre);
		if(categoria.isPresent()) {
			throw new CategoriaYaExisteException(categoria.get().getNombre());
		}
	}

}
