package com.ligasbarriales.servicio.Impl;

import static com.ligasbarriales.utils.UtilidadesCadena.normalizarNombre;

import java.util.List;
import java.util.Optional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ligasbarriales.exception.EquipoYaExisteException;
import com.ligasbarriales.modelo.Equipo;
import com.ligasbarriales.repositorio.RepositorioEquipo;
import com.ligasbarriales.servicio.IEquipoServicio;

@Service
public class EquipoServicioImpl implements IEquipoServicio {

	private static final Log LOG = LogFactory.getLog(EquipoServicioImpl.class);

	@Autowired
	private RepositorioEquipo repositorioEquipo;
	
	@Override
	public Equipo registrar(Equipo obj) {
		
		asegurarUnicoEquipo(obj.getNombre());		
		String nombreNormalizado = asegurarNombre(obj.getNombre());
		
		Equipo equipo = new Equipo(nombreNormalizado, obj.getAnioFundacion());
		LOG.info("Guardando Equipo nuevo: "+ equipo.toString());
		return repositorioEquipo.save(equipo);
	}

	@Override
	public Equipo modificar(Equipo obj) {
		LOG.info("Actualizando Equipo: "+ obj.toString());
		return repositorioEquipo.save(obj);
	}

	@Override
	public List<Equipo> listar() {
		LOG.info("Buscando Equipos... ");
		List<Equipo> equiposTodos = (List<Equipo>) repositorioEquipo.findAll();
		LOG.info(String.format("Obteniendo %s Equipos: ", equiposTodos.size()));
		return equiposTodos;
	}

	@Override
	public Optional<Equipo> listarPorId(Long id) {
		LOG.info(String.format("Buscando a Equipo por Id %s. ",id));
		return repositorioEquipo.findById(id);
	}

	@Override
	public boolean eliminar(Long id) {
		LOG.info(String.format("Eliminando a Equipo con id %s. ",id));
		repositorioEquipo.deleteById(id);
		return true;
	}
	
	private String asegurarNombre(String nombre) {
		return normalizarNombre(nombre);	
	}
	
	private void asegurarUnicoEquipo(String nombre){
		Optional<Equipo> equipo = repositorioEquipo.findByNombre(nombre);
		if(equipo.isPresent()) {
			throw new EquipoYaExisteException(equipo.get().getNombre());
		}
	}

}
