package com.ligasbarriales.servicio;

import com.ligasbarriales.modelo.Persona;

public interface IPersonaServicio extends ICRUD<Persona, Long> {

}
