package com.ligasbarriales.utils;

import java.util.regex.Pattern;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;


public class UtilidadesSeguridad {


	private static final Pattern PATRON_CONTRASENA = Pattern.compile("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(\\S){8,20}$");

	private static final Authentication authentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}
	
	private final static boolean authenticationCorrespondeAUsuarioAnonimo(Authentication authentication) {
		return authentication.getName().equals("anonymousUser");
	}
	
	public static final boolean contrasenaCumplePatron(String contrasena) {
		return PATRON_CONTRASENA.matcher(contrasena).matches();
	}

	public static final boolean esUsuarioAnonimo() {
		final Authentication authentication = authentication();
		return (authentication != null) && (authenticationCorrespondeAUsuarioAnonimo(authentication));
	}
	
	public static final boolean esUsuarioIdentificado() {
		final Authentication authentication = authentication();
		return (authentication != null) && (!authenticationCorrespondeAUsuarioAnonimo(authentication));
	}
	
	public static final String generarContrasenaAleatoriaAlfanumerica(final int longitud) {
		return RandomStringUtils.randomAlphanumeric(longitud);
	}
	
	/**
	 * La longitud suministrada debe estar en el rango permitido por el patrón, de otra forma se generará un bucle infinito.
	 */
	public static final String generarContrasenaAleatoriaAlfanumericaQueCumplaPatron(final int longitud) {
		
		String contrasena;
		
		do {
			contrasena = generarContrasenaAleatoriaAlfanumerica(longitud);
		} while (!contrasenaCumplePatron(contrasena));
		
		return contrasena;
	}
	
	public static final String nombreUsuarioEnSesion() {
		return authentication().getName();
	}
	
//	public static final boolean contraseniaDefault() {
//		MembresiaPersonaTitular titular = usuarioEnSesionSocio();
//		return titular.isContrasenaPorDefecto();
//	}
	
//	private static final boolean rolEs(RolUsuario rol) {
//		return authentication().getAuthorities().stream()
//				.anyMatch((authority) -> authority.getAuthority().equals(rol.name()));
//	}
	
	public static final String usuarioEnSesion() {
		return (String)authentication().getPrincipal();
	}

	private UtilidadesSeguridad() {}
}
