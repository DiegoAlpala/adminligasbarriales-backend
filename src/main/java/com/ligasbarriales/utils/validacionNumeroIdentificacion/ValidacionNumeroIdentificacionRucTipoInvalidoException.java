package com.ligasbarriales.utils.validacionNumeroIdentificacion;

@SuppressWarnings("serial")
public class ValidacionNumeroIdentificacionRucTipoInvalidoException extends ValidacionNumeroIdentificacionException {

	private RucTipo tipo;
	
	public ValidacionNumeroIdentificacionRucTipoInvalidoException(RucTipo tipo) {
		this.tipo = tipo;
	}

	@Override
	public String getMessage() {
		return "El tipo es inválido: " + tipo;
	}
}
