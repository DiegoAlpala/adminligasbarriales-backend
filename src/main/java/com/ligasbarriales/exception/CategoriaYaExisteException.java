package com.ligasbarriales.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
@SuppressWarnings("serial")
public class CategoriaYaExisteException extends CategoriaException {

	private String nombre;

	public CategoriaYaExisteException(String nombre) {
		this.nombre = nombre;
	}
	
	@Override
	public String getMessage() {
		return String.format("La categoría '%s' ya existe.", nombre);
	}
}
