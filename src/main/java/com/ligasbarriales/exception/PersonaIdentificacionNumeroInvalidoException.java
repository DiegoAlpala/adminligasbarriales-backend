package com.ligasbarriales.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
@SuppressWarnings("serial")
public class PersonaIdentificacionNumeroInvalidoException extends PersonaException {

	private String numeroIdentificacion;

	public PersonaIdentificacionNumeroInvalidoException(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}
	
	@Override
	public String getMessage() {
		return String.format("El número de identificación es inválido: '%s'", numeroIdentificacion);
	}
}
