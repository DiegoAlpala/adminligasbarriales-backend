package com.ligasbarriales.exception;

@SuppressWarnings("serial")
public class PersonaIdentificacionNumeroYaExisteException extends PersonaException {

	private String numeroIdentificacion;

	public PersonaIdentificacionNumeroYaExisteException(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}
	
	@Override
	public String getMessage() {
		return String.format("Persona ya existe con número de identificación: '%s'", numeroIdentificacion);
	}
}
