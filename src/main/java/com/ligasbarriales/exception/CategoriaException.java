package com.ligasbarriales.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
@SuppressWarnings("serial")
public abstract class CategoriaException extends RuntimeException {

	public abstract String getMessage();
}
