package com.ligasbarriales.exception;

@SuppressWarnings("serial")
public abstract class PersonaException extends RuntimeException {

	public abstract String getMessage();
}
