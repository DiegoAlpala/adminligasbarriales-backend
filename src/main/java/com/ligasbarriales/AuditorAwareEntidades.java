package com.ligasbarriales;

import static com.ligasbarriales.utils.UtilidadesCadena.truncar;
import static com.ligasbarriales.utils.UtilidadesSeguridad.esUsuarioAnonimo;
import static com.ligasbarriales.utils.UtilidadesSeguridad.esUsuarioIdentificado;
import static com.ligasbarriales.utils.UtilidadesSeguridad.usuarioEnSesion;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.userdetails.UserDetails;

public class AuditorAwareEntidades implements AuditorAware<String> {
	
	private static final int AUDITORIA_LONGITUD_USUARIO_MAX = 100;
	private static final String AUDITORIA_USUARIO_DESCONOCIDO = "desconocido";
	private static final String AUDITORIA_USUARIO_ANONIMO = "anonimo";
	
	@Override
	public Optional<String> getCurrentAuditor() {
		
		if (esUsuarioIdentificado()) {
			return usuarioDeSesion();
		} else if (esUsuarioAnonimo()) {
			return Optional.of(AUDITORIA_USUARIO_ANONIMO);
		} else {
			return Optional.of(AUDITORIA_USUARIO_DESCONOCIDO);
		}
	}

	private final Optional<String> usuarioDeSesion() {
		final String userDetails = usuarioEnSesion();
		final String usuario = String.format("%s", userDetails);
		return Optional.of(truncar(usuario, AUDITORIA_LONGITUD_USUARIO_MAX));
	}
}
